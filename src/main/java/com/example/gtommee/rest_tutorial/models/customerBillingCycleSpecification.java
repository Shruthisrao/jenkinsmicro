package com.example.gtommee.rest_tutorial.models;

public class customerBillingCycleSpecification {

	private String id;
	private String href;
	private String name;
	private int billingDateShift;
	private String frequency;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBillingDateShift() {
		return billingDateShift;
	}
	public void setBillingDateShift(int billingDateShift) {
		this.billingDateShift = billingDateShift;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	

}
