package com.example.gtommee.rest_tutorial.models;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.gtommee.rest_tutorial.models.Bill;
import com.example.gtommee.rest_tutorial.repositories.BillRepository;

@RestController
@RequestMapping("/billing")
public class BillMain {

	//@Autowired
		//private PetsRepository repository;
		
		@Autowired
		private BillRepository rep;
		
		//get method
		@RequestMapping(value = "/", method = RequestMethod.GET)
		public List<Bill> getAllBill(){
			return rep.findAll();
			
		}
		@RequestMapping(value = "/{id}", method = RequestMethod.GET)
		public Optional<Bill> getPetById(@PathVariable("id") String id) {
		 // return rep.findBy_id(id);
			return rep.findById(id);
			
		}
		@RequestMapping(value ="/{Id}", method = RequestMethod.DELETE)
		public String deleteBill(@PathVariable String Id) {
			System.out.println("id"+Id);
			rep.deleteById(Id);
			return ("success");
		}
		
		@RequestMapping(value = "/{Id}", method =RequestMethod.PUT)
		public Bill updateId(@PathVariable("Id") String id, @RequestBody Bill body) {
			System.out.println(id);
			body.set_id(id);
			rep.save(body);
			return body;
			
		}
		@RequestMapping(value = "/{Id}", method = RequestMethod.POST)
		public Bill createPet(@Valid @RequestBody Bill billing) {
		//billing.set_id(ObjectId.get());
		  rep.save(billing);
		  return billing;
		}
		
		
		/*@RequestMapping(method = RequestMethod.POST)
		public troubleticket createTroubleTicket( @RequestBody troubleticket tbody) {
			Date now = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
			String strDate = dateFormat.format(now); 
			System.out.println("DATE"+strDate);
			tbody.setCreationDate(strDate);
			rep.save(tbody);
			return tbody;
			
			
		}
		
		*/
		
		
		
}
